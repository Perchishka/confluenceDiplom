import {goto, run, stop} from '../../lib/driver/driver'
import chai from 'chai';
import {urls} from "../../framework/config/urls";

import {app} from "../../framework";
import {credentials} from "../../framework/config/credentials";
import {describe} from "mocha";
import { allure } from 'allure-mocha/runtime'


const {expect} = chai;

let page;
let uiApp;
const wrongEmail = 'abc';
const wrongPassword = 'abc';
describe('Login and LogOut suit', () => {

    beforeEach(async () => {
        await run();
        page = await goto(urls.confluence);
        uiApp = app(page).uiProvider;
    });

    afterEach(async () => {
        await stop();
    })
    it('Successful login', async () => {
        await uiApp.LoginPage().login(credentials.email, credentials.password)
        const header =await uiApp.MainPage().getHeaderText();
        expect(header).to.equal('Ваша работа');
    })
    it('Successful logout', async () => {
        await uiApp.LoginPage().login(credentials.email, credentials.password)
        await uiApp.UpperMenu().logOut();
        await uiApp.LogOutPage().clickOnLogOutButton();
        expect( await uiApp.LoginPage().emailField).to.exist;

    })
    it ('Email without @ sign, check that password field is hidden', async () => {
        await uiApp.LoginPage().enterEmail(wrongEmail);
        const passWordField = await uiApp.LoginPage().getPasswordParentClass();
        expect(passWordField).to.include('hidden');
    })
    it ('Wrong password', async () => {
        await uiApp.LoginPage().enterEmail(credentials.email);
        await uiApp.LoginPage().enterPassword(wrongPassword);
        const errorMessage = await uiApp.LoginPage().getErrorMessage();
        expect(errorMessage).to.include('Неверный адрес электронной почты и/или пароль.');
    })

    it ('Empty password', async () => {
        await uiApp.LoginPage().enterEmail(credentials.email);
        await uiApp.LoginPage().clickOnSubmitButton();
        const errorMessage = await uiApp.LoginPage().getPasswordErrorMessage();
        expect(errorMessage).to.include('Введите пароль');
    })
})

