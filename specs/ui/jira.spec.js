import {goto, run, stop} from '../../lib/driver/driver'
import chai from 'chai';
import {urls} from "../../framework/config/urls";
import faker from 'faker';
import {app} from "../../framework";
import {credentials} from "../../framework/config/credentials";
import {describe} from "mocha";
import { allure } from 'allure-mocha/runtime'


const {expect} = chai;

let page;
let uiApp;
const projectName = 'ScrumProject';
const backlogButton = 'Бэклог';

describe('Jira suit', () => {

    beforeEach(async () => {
        await run();
        page = await goto(urls.confluence);
        uiApp = app(page).uiProvider;
        await uiApp.LoginPage().login(credentials.email, credentials.password)
        await uiApp.UpperMenu().clickOnAppSwitcher();
        await uiApp.UpperMenu().clickOnMenuItem('Jira Software');
    });

    afterEach(async () => {
        await stop();
    });
    it('Click on "Jira Software" button and go to "Project page" ', async () => {
        const header =await uiApp.JiraProjectsPage().getProjectPageHeader();
        expect(header).to.equal('Проекты - Jira');
    });
    it('Create a sprint in Jira', async () => {
        await uiApp.JiraProjectsPage().getProjectPageHeader();
        await uiApp.JiraProjectsPage().clickProjectName(projectName);
        await uiApp.JiraProjectLeftMenu().clickOnLeftMenuButton(backlogButton);
        await uiApp.BackLogJiraPage().clickOnCreateSprintButton();
        const sprintNames = await uiApp.BackLogJiraPage().gettingNameListOfWebElement();
        expect(sprintNames).to.have.lengthOf(2);
    });
    it ('Delete sprint', async () => {
        await uiApp.JiraProjectsPage().getProjectPageHeader();
        await uiApp.JiraProjectsPage().clickProjectName(projectName);
        await uiApp.JiraProjectLeftMenu().clickOnLeftMenuButton(backlogButton);
        await uiApp.BackLogJiraPage().performActionWithSprint('Доска Спринт 2', 'Удалить спринт');
        const sprintNames = await uiApp.BackLogJiraPage().gettingNameListOfWebElement();
        expect(sprintNames).not.to.have.property('Доска Спринт 2')
    });
    it ('Create ticket in backlog', async () => {
        const summary = faker.name.jobDescriptor();
        await uiApp.JiraProjectsPage().getProjectPageHeader();
        await uiApp.JiraProjectsPage().clickProjectName(projectName);
        await uiApp.JiraProjectLeftMenu().clickOnLeftMenuButton(backlogButton);
        await uiApp.BackLogJiraPage().createTicketInBackLog(summary);
        expect(await uiApp.BackLogJiraPage().backLogCreatedTicket(summary)).to.exist;
    });
    it ('Check that ticket from backlog can be deleted', async () => {
        await uiApp.JiraProjectsPage().getProjectPageHeader();
        await uiApp.JiraProjectsPage().clickProjectName(projectName);
        await uiApp.JiraProjectLeftMenu().clickOnLeftMenuButton(backlogButton);
        const tickets = await uiApp.BackLogJiraPage().getBackLogTicketsNames();
        const firstTicket = tickets[0];
        await uiApp.BackLogJiraPage().clickOnExistingTicket(firstTicket);
        await uiApp.RightTicketMenu().deleteTicket();

    })
})

