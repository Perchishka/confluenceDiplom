import {app} from "../../framework";
import chai from "chai";
import faker from 'faker';
import { allure } from 'allure-mocha/runtime'

const {expect} = chai;

let apiApp;
describe('Confluence space test suit', () => {
    beforeEach(async () => {
        apiApp = app().apiProvider;
    });

    it('Get space', async () => {
        const {results} = await apiApp.space().getSpace();
        console.log(results)
        const neededSpace = results.find(({name}) => name === 'JavaScriptCourse');
        expect(neededSpace.key).to.equal('JAVASCRIPT');
    });

    it('Create space', async () => {
        const key = 'Key' + Math.floor(Math.random() * 101);
        const name = faker.name.lastName();
        const r = await apiApp.space().createSpace(key, name);
        expect(r.key).to.equal(key);
        expect(r.name).to.equal(name);
    });


});
