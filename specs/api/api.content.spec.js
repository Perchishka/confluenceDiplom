import {app} from "../../framework";
import chai from "chai";
import faker from 'faker';
import {credentials} from "../../framework/config/credentials";
import { allure } from 'allure-mocha/runtime'
const {expect} = chai;

let apiApp;
describe('Confluence api content positive test suit', () => {

    beforeEach(async () => {
        apiApp = app().apiProvider;
    });

    it('Create page and check that it was created in the right space', async () => {
        const key = 'Key98';
        const pageTitle = 'Title ' + faker.name.lastName();
        const textBody = "<p>This is <br/> a new page</p>";
        const {space, title, status} = await apiApp.content().createPage(key, pageTitle, textBody);
        expect(title).to.equal(pageTitle);
        expect(space.key).to.equal(key);
        expect(status).to.equal('current');

    });

    it('Create child page and check that it was created in the right space', async () => {
        const key = 'Key98';
        const pageTitle = 'Title ' + faker.name.lastName();
        const textBody = "<p>This is <br/> a new child page</p>";
        const parentId ='29523979';
        const {space, title, status, ancestors} = await apiApp.content().createChildPage(key, pageTitle, textBody, parentId);
        expect(title).to.equal(pageTitle);
        expect(space.key).to.equal(key);
        expect(status).to.equal('current');
        const ancestorIds = ancestors.find(({id}) => id === parentId);
        expect(ancestorIds.title).to.equal('Title Keebler');
    });

    it('Get page by spaceKey and title', async () => {
        const key = 'Key98';
        const pageTitle = 'Title Keebler';
        const {results} = await apiApp.content().getPage(key, pageTitle);
        const neededPage = results.find(({title}) => title === pageTitle);
        expect(results.length).to.equal(1);
        expect(neededPage.id).to.equal('29523979');
    });

    it('Get page by Id', async () => {
        const id = '29523979';
        const {title} = await apiApp.content().getPageById(id);
        expect(title).to.equal('Title Keebler');
    });

    it('Get page history', async () => {
        const id = '29523979';
        const {createdBy} = await apiApp.content().getHistory(id);
        expect(createdBy.email).to.equal(credentials.email);
    });

});
