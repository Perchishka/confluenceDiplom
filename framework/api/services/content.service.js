import {ancestors, pageBody, pageJson} from "../../config/pageJson";
import * as apiHelper from "../../../lib/apiHelper";

function Content() {
    let entity = 'content';
    let restUrl;

    this.createPage = async function createPage(key, title, textBody) {
        const page = pageJson(key, title);
        const body = pageBody(textBody);
        const combinedPageJson = {...page, ...body}
        return await apiHelper.post(combinedPageJson, entity);
    }

    this.createChildPage = async function createChild(key, title, textBody, id){
        const page = pageJson(key, title);
        const body = pageBody(textBody);
        const ancestor = ancestors(id);
        const combinedPageJson = {...page, ...ancestor, ...body }
        console.log(combinedPageJson);
        return await apiHelper.post(combinedPageJson, entity);
    }

    this.getPage = async function getPage(key, title) {
        restUrl = `?&spaceKey=${key}&title=${title}&expand=space,body.view,version,container`;
        return await apiHelper.get(entity, restUrl);
    }

    this.getPageById = async function getPageById(id) {
        restUrl = `/${id}?expand=space,body.view,version,container`;
        return await apiHelper.get(entity, restUrl);
    }

    this.getHistory = async function getHistory(id) {
        restUrl = `/${id}/history`;
        return await apiHelper.get(entity, restUrl);
    }

}

export {Content};
