import fetch from 'node-fetch';
import {spaceJson} from "../../config/spaceJson";
import * as apiHelper from "../../../lib/apiHelper";

function Space() {
    let entity = 'space';
    let restUrl;

    this.getSpace = async function getSpace() {
        restUrl = '';
        return await apiHelper.get(entity, restUrl);
    }

    this.createSpace = async function createSpace(key, name) {
        return await apiHelper.post(spaceJson(key, name), entity);
    }
}

export {Space};
