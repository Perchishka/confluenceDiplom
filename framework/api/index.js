import {Space} from './services/space.service'
import {Content} from "./services/content.service";

const apiProvider = () => ({
    space: () => new Space(),
    content: () => new Content(),
});

export {apiProvider};
