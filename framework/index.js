import {apiProvider} from "./api";
import {uiProvider} from "./ui";


const app = (page) => ({
    apiProvider: apiProvider(),
    uiProvider:  uiProvider(page),

});

export { app };
