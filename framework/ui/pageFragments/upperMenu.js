import {BasePage} from "../pages/basePage";

export class UpperMenu extends BasePage {
    constructor(page) {
        super(page);

        this.accountButton = '//header[@role="banner"]//span[last()]//button';
        this.logoutButton = 'a[href="/logout"]';
        this.appSwitcher = '[aria-label="Appswitcher Icon"]';
        this.createNewButton ='button#createGlobalItemIconButton';
        this.dropdownMenu = (item) => `//div[@data-placement="bottom-start"]//span[contains(text(),"${item}")]`
    }

    async logOut() {
        await this.page.waitForSelector(this.accountButton);
        await this.page.hover(this.accountButton);
        await this.page.click(this.accountButton);
        await this.page.hover(this.logoutButton);
        await this.page.click(this.logoutButton);
    }

    async clickOnMenuItem(item) {
        await this.page.hover(this.dropdownMenu(item));
        await this.page.click(this.dropdownMenu(item));
    }

    async clickOnAppSwitcher() {
        await this.page.click(this.appSwitcher);
    }

    async clickOnCreateItemButton(){
        await this.page.click(this.createNewButton);
    }


}
