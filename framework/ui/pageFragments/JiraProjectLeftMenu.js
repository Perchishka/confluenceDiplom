import {BasePage} from "../pages/basePage";

class JiraProjectLeftMenu extends BasePage {

    constructor(page) {
        super(page);

        this.leftMenuButton = (buttonName) => `//span[contains(text(), "${buttonName}")]`;

    }

    async clickOnLeftMenuButton(buttonName){
        await this.page.click(this.leftMenuButton(buttonName));
    }

}

export { JiraProjectLeftMenu };
