import {BasePage} from "../pages/basePage";

class RightTicketMenu extends BasePage{
    constructor(page) {
        super(page);

        this.ticketActionButton ='[data-testid="issue-meatball-menu.ui.dropdown-trigger.button"]';
        this.actionLocator = (menuElement) => `//div[@data-placement="left-end"]//span[contains(text(), "${menuElement}")]`;
        this.confirmAction = (menuElement) => `//div[@role="dialog"]//footer//span[contains(text(), "${menuElement}")]`;
    }

    async performAction(choseAction){
        await this.page.waitForSelector(this.ticketActionButton);
        await this.page.click(this.ticketActionButton);
        await this.page.click(this.actionLocator(choseAction));
    }

    async deleteTicket(){
        await this.performAction("Удалить");
        await this.page.click(this.confirmAction("Удалить"));
        await this.page.reload();

    }
}

export {RightTicketMenu};
