import {BasePage} from "../pages/basePage";

class CreateJiraTicketFragment extends BasePage {

    constructor(page) {
        super(page);

        this.summary = '#summary';
        this.description = '#description';
        this.priority = '';
        this.tags = '#labels-textarea';
        this.createButton = '';
        this.sprint = '';
        this.epicLink ='';
        this.executor = '';
        this.relatedTasks = '';
    }

}

export { CreateJiraTicketFragment };
