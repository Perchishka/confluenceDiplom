import {MainPage} from './pages/mainPage';
import {LoginPage} from './pages/loginPage';
import {UpperMenu} from './pageFragments/upperMenu'
import {LogoutPage} from './pages/logOutPage';
import {JiraProjectsPage} from "./pages/jiraProjectsPage";
import {CreateJiraTicketFragment} from "./pageFragments/createJiraTicketFragment";
import {JiraProjectLeftMenu} from "./pageFragments/JiraProjectLeftMenu";
import {BackLogJiraPage} from "./pages/backLogJiraPage";
import {RightTicketMenu} from "./pageFragments/rightTicketMenu";

const uiProvider = (page) => ({
    MainPage: () => new MainPage(page),
    LoginPage: () => new LoginPage(page),
    UpperMenu: () => new UpperMenu(page),
    LogOutPage: () => new LogoutPage(page),
    JiraProjectsPage: () => new JiraProjectsPage(page),
    CreateJiraTicketFragment: () => new CreateJiraTicketFragment(page),
    JiraProjectLeftMenu: () => new JiraProjectLeftMenu(page),
    BackLogJiraPage: () => new BackLogJiraPage(page),
    RightTicketMenu: () => new RightTicketMenu(page),
});


export {uiProvider};
