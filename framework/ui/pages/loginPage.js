import { BasePage} from "./basePage";
import {UiHelper} from "../../../lib/uiHelper";

class LoginPage extends BasePage {
    constructor(page) {
        super(page);

        this.emailField = '#username';
        this.submitButton = 'button[type="submit"]';
        this.passwordField = '#password';
        this.loginButton = '#login-submit';
        this.passwordAncestor = '//input[@id="password"]/ancestor::div[contains(@class, "field-group")]'
        this.commonErrorMessage = '#login-error span';
        this.passwordErrorMessage = '#password-error';
    }

    async getErrorMessage(){
        return await this.page.textContent(this.commonErrorMessage)
    }

    async getPasswordErrorMessage(){
        return await this.page.textContent(this.passwordErrorMessage)
    }

    async login(username, password){
        await this.page.fill(this.emailField, username);
        await this.page.click(this.submitButton);
        await this.page.fill(this.passwordField, password);
        await this.page.click(this.loginButton);
        await this.page.waitForNavigation;
    }

    async enterEmail(username){
        await this.page.fill(this.emailField, username);
        await this.page.click(this.submitButton);
    }

    async enterPassword(password){
        await this.page.fill(this.passwordField, password);
        await this.page.click(this.loginButton);
    }

    async clickOnSubmitButton(){
        await this.page.click(this.loginButton);
    }


    async getPasswordParentClass(){
        return await UiHelper.getClass(this.page, this.passwordAncestor);
     }

}

export { LoginPage };
