import {BasePage} from "./basePage";

export class LogoutPage extends BasePage{
    constructor(page){
        super(page);

        this.logoutButton ='#logout-submit';
    }

    async clickOnLogOutButton(){
        await this.page.waitForSelector(this.logoutButton);
        await this.page.click(this.logoutButton);
    }
}
