import {BasePage} from "./basePage";
import {UiHelper} from "../../../lib/uiHelper";

class BackLogJiraPage extends BasePage{
    constructor(page) {
        super(page);
        this.addSprintButton ='.js-add-sprint';
        this.sprintsList ='.ghx-sprint-group>div[data-sprint-id] .ghx-name';
        this.sprintDropDownMenuButton = (sprintName)  =>`//div[contains(text(), '${sprintName}')]/ancestor::div[contains(@class, 'js-sprint-header')]/div[@class='header-right']//a[contains(@class, 'js-sprint-actions-trigger')]`;
        this.sprintDropDownOption = (action) => `//div[contains(@class, 'js-sprint-actions-list')]//a[contains(text(), '${action}')]`;
        this.approveDeletingButton = '#ghx-remove-planned-sprint-confirm-dialog button.button-panel-button';
        this.createTicketInBackLogButton = '.ghx-backlog-group .iic-trigger';
        this.backlogTicketSummaryField = '.ghx-backlog-group .iic-widget__summary';
        this.backLogCreatedTicket = (ticketTitle) =>`//div[contains(@class, "ghx-backlog-container")]//span[contains(text(), "${ticketTitle}")]`
        this.backlogTicketsList ='//div[contains(@class, "ghx-backlog-container")]//span[@class="ghx-inner"]'

    }

    async clickOnCreateSprintButton(){
        await this.page.click(this.addSprintButton);
        await this.page.reload();
    };

    async gettingNameListOfWebElement() {
       return await UiHelper.getListOfText(this.page, this.sprintsList);
    };

    async getBackLogTicketsNames(){
       return await UiHelper.getListOfText(this.page, this.backlogTicketsList);
    };

    async performActionWithSprint(sprintName, action){
        await this.page.click(this.sprintDropDownMenuButton(sprintName));
        await this.page.hover(this.sprintDropDownOption(action));
        await this.page.click(this.sprintDropDownOption(action));
        await this.page.click(this.approveDeletingButton);
    };

    async createTicketInBackLog(summary){
        await this.page.click(this.createTicketInBackLogButton);
        await this.page.fill(this.backlogTicketSummaryField, summary);
        await this.page.keyboard.press('Enter');
        await this.page.reload();
        await this.page.waitForSelector(this.addSprintButton);
    };

    async clickOnExistingTicket(ticketTitle){
        await this.page.click(this.backLogCreatedTicket(ticketTitle));
    }
}

export {BackLogJiraPage};
