import { BasePage} from "./basePage";
import {UiHelper} from "../../../lib/uiHelper";

class MainPage extends BasePage {
    constructor(page) {
        super(page);

        this.projectRedirectionButton = 'a[href*="/browse/SCRUM"]';
        this.header = '#ak-main-content h1';
        this.createButton = '#createGlobalItem';
        this.accountButton ='button[class="css-74qtiq"]';
    }

    async openProject(){
         await this.page.click(this.projectRedirectionButton);
    }

    async getHeaderText(){
        return await UiHelper.getText(this.page, this.header);
    }

}

export { MainPage };
