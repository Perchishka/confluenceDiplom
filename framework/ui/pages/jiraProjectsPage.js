import {BasePage} from "./basePage";


class JiraProjectsPage extends BasePage {
    constructor(page) {
        super(page);

        this.projectHeader = 'div>h1';
        this.projectTableRow = (projectName) => `//div[@data-test-id="global-pages.directories.directory-base.content.table.container"]//span[contains(text(), "${projectName}")]`;
    }

    async clickProjectName(projectName) {
        await this.page.click(this.projectTableRow(projectName));
    }

    async getProjectPageHeader(){
       return await this.page.title();
    }
}

export {JiraProjectsPage};
