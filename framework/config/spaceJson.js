import faker from 'faker';

function spaceJson (key, name) {
    return{
        key: key,
        name: name,
        type: 'global',
        description: description()
    }
}

function description() {
    return {
        plain: {
            value: "Raider Space for raiders",
            representation: "plain"
        },
    }
}

export { spaceJson };
