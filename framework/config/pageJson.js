function pageJson(key, title) {
    return {
        type: "page",
        title: title,
        space: {
            key: key
        }
    }
}

function pageBody(bodyValue) {
    return {
        body:{
        storage: {
            value: bodyValue,
            representation: "storage"
        }
        },
    }
}

function ancestors(id){
    return{
        ancestors: [{
          id: id
        }]
    }
}

export {pageJson, pageBody, ancestors}
