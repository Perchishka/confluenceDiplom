const UiHelper = {

    getText: async function (page, element) {
        await page.waitForSelector(element);
        return await page.innerText(element);
    },

    getListOfText: async function (page, selector) {
        await page.waitForSelector(selector)
        let elements = await page.$$(selector);
        let texts = [];
        for (let i = 0; i < elements.length; i++) {
            const element = elements[i];
            const t = await element.innerText();
            texts.push(t);
        }
        return texts;
    },

    checkCheckbox: async function (page, selector) {
        await page.waitForSelector(selector);
        const ifCheck = await page.isChecked(selector);
        !ifCheck ? await page.check(selector) : console.log("Checkbox is already checked");
    },

    optionFromSelect: async function (page, selector, option) {
        await page.waitForSelector(selector, {state: 'attached'});
        await page.selectOption(selector, {label: option});
    },

    forceClick: async function (page, selector) {
        page.click(selector, {force: true});
    },

    getClass: async function (page, selector) {
        return await page.getAttribute(selector, 'class');
    }
}


export {UiHelper};
