export function OS() {

    let osName = "Unknown OS";

    const platform = process.platform.toLowerCase();
    if (platform.indexOf("win") !== -1) osName = 'Windows';
    if (platform.indexOf("darwin") !== -1) osName = 'MacOS';
    if (platform.indexOf("bsd") !== -1) osName = 'UNIX';
    if (platform.indexOf("linux") !== -1) osName = 'Linux';

    return osName;

}

export function headlessSwitch(osName) {
    switch (osName) {
        case 'Windows':
            return false;
        case 'UNIX':
        case 'Linux':
        case 'MacOS':
            return true;
        default:
            return false;
    }
}



