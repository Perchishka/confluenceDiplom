import playwright from 'playwright';
import {headlessSwitch, OS} from "./operatingSystem";


let browser;
let context;
let page;

async function goto(url) {
    await page.goto(url)
    return page;
}

async function run() {
    const detectedOs = OS();
    const useHeadless = headlessSwitch(detectedOs);
    console.log({detectedOs, useHeadless});
    browser = await playwright.chromium.launch({
        headless: useHeadless,
        slowMo: 250,
    });
    context = await browser.newContext();
    page = await context.newPage();
    await page.setViewportSize({
        width: 1900,
        height: 800,
    });

};


async function stop() {
    await page.close();
    await browser.close();
};

export {goto, run, stop}
