import fetch from "node-fetch";
import { credentials } from "../framework/config/credentials";
import { urls } from "../framework/config/urls";

const auth = 'Basic ' + Buffer.from(`${credentials.email}:${credentials.apiToken}`).toString('base64');
const headers = {Authorization: auth, 'Content-Type': 'application/json'};

async function get(entity, restUrl) {
    const url = `${urls.confluence}wiki/rest/api/${entity}${restUrl}`;
    const response = await fetch(url, {
        method: 'GET',
        headers: headers,
    });
    if (!response.ok) throw new Error(`unexpected response:  ${response.statusText}`);
    return response.json();

}

async function post(jsonBody, entity) {
    const response = await fetch(`${urls.confluence}wiki/rest/api/${entity}`, {
        method: 'POST',
        body: JSON.stringify(jsonBody),
        headers: headers,
    });
    if (!response.ok) throw new Error(`unexpected response:  ${response.statusText}`);
    return response.json();
}

export {get, post}
